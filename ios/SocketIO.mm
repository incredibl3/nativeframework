#import "SocketIO.h"

@implementation SocketIOPlugin

// The plugin must call super dealloc.
- (void) dealloc {
    self.appDelegate = nil;
    
    [super dealloc];
}

// The plugin must call super init.
- (id) init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.appDelegate = nil;
    
   
    return self;
}

+ (SocketIOClient *) connect:(NSString * __nonnull)socketURL options:(NSDictionary * __nullable)options
{
    SocketIOClient* socket = [[SocketIOClient alloc] initWithSocketURL:socketURL options:options];
    return socket;
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
    }];
}

@end