#import "PluginManager.h"
#import <TeaLeafIOS-Swift.h>

@interface SocketIOPlugin: GCPlugin

@property (nonatomic, retain) TeaLeafAppDelegate *appDelegate;

+(SocketIOClient *) connect:(NSString * __nonnull)socketURL options:(NSDictionary * __nullable)options;

@end