#import "Dialog.h"

@implementation NativeDialogPlugin

// The plugin must call super dealloc.
- (void) dealloc {
    self.appDelegate = nil;
    
    [super dealloc];
}

// The plugin must call super init.
- (id) init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.appDelegate = nil;
   
    return self;
}

- (void) showDialog:(NSDictionary *)jsonObject {
	NSArray *msg = [jsonObject objectForKey:@"messages"];
    NSString *text = [msg componentsJoinedByString:@"\r"];

	TeaLeafViewController* controller = (TeaLeafViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];

    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:jsonObject[@"title"]
                                  message:text
                                  preferredStyle:UIAlertControllerStyleAlert];

    // Add button :D
    NSArray *buttons = [jsonObject objectForKey:@"buttons"];
    for(int i = 0; i < [buttons count]; i++) {
            UIAlertAction *action = [UIAlertAction
                                    actionWithTitle:buttons[i]
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction *action) {
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        [[PluginManager get] dispatchJSEvent:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                              @"nativedialog",@"name", [NSNumber numberWithInteger:i], @"buttonPressed", nil]];

                                    }];
            [alert addAction:action];
    }

    [controller presentViewController:alert animated:YES completion:nil];
}

@end