#import "ParsePlugin.h"
#import <Parse/Parse.h>

@implementation ParsePlugin

// The plugin must call super dealloc.
- (void) dealloc {
    self.appDelegate = nil;
    
    [super dealloc];
}

// The plugin must call super init.
- (id) init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.appDelegate = nil;
   
    return self;
}

- (void) initializeWithManifest:(NSDictionary *)manifest appDelegate:(TeaLeafAppDelegate *)appDelegate {
    @try {
        NSLog(@"{parse} Initializing with manifest");
        
        self.appDelegate = appDelegate;

        // Enable storing and querying data from Local Datastore. Remove this line if you don't want to
        // use Local Datastore features or want to use cachePolicy.
        [Parse enableLocalDatastore];

        // Uncomment and fill in with your Parse credentials:
        [Parse setApplicationId:@"UJZ2OlusGRE01wEIuLkG4UjBAgzyQCB5l2Zf7fkL" clientKey:@"J3kEwPYFbU7G1h85RbPLHyOgMCQvofkZ5EinJiRS"];


        [PFUser enableAutomaticUser];
        PFACL *defaultACL = [PFACL ACL];
        
        // If you would like all objects to be private by default, remove this line.
        [defaultACL setPublicReadAccess:YES];

        [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];

    }
    @catch (NSException *exception) {
        NSLog(@"{parse} Failure to get ios:moPubID key from manifest file: %@", exception);
    }
}

- (void) reloadData:(NSDictionary *) jsonObject {
    @try {
        PFQuery *query = [PFQuery queryWithClassName:@"HighScore"];
        [query whereKey:@"finishTime" greaterThan:@(0)];
        [query orderByAscending:@"finishTime"];
        [query setLimit:10];

        int problems = [jsonObject[@"problems"] intValue];
        if (problems != -1) {
            [query whereKey:@"problems" equalTo:@(problems)];
        }

        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {

            if (!error)
            {
                NSLog(@"{parse} objectsWereRetrievedSuccessfully");
                int rank = 1;
                for (id element in objects){
                    NSLog(@"%@",element[@"name"]);
                    [[PluginManager get] dispatchJSEvent:[NSDictionary dictionaryWithObjectsAndKeys:                                                      @"parseevent",@"name",
                        [NSNumber numberWithInt:rank], @"rank",
                        [NSNumber numberWithInt:problems], @"problems",
                        element[@"name"], @"player_name",
                        [NSNumber numberWithDouble:[element[@"finishTime"] doubleValue]], @"score", nil]];
                    rank++;
                }

            } else {
                NSLog(@"{parse} objectRetrievalFailed");
            }

        }];
    }
    @catch (NSException *exception) {

    }
}

@end