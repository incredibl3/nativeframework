#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#include "WebViewPlugin.h"

@interface InGameBrowserNative : NSObject<UIWebViewDelegate>{
    UIView*                     _IGBView;
    UINavigationBar*            _navBar;
    UIBarButtonItem*            _navBarBack;
    
    UIToolbar*                  _toolbar;
	UIBarButtonItem*            _reloadButton;
	UIBarButtonItem*            _loadingButton;
	UIBarButtonItem*            _forwardButton;
	UIBarButtonItem*            _backButton;
	UIBarButtonItem*            _flexibleSpace;
    UIBarButtonItem*            _fixedSpace;

    UIActivityIndicatorView*    _activityIndicatorRedirect;
    UIWebView*                  _IGBWebView;   

    CGFloat         _screenWidth;
    CGFloat         _screenHeight;

    UIView*         _parentView;    
}

//parent view of the In-game Browser. Usually gameView
@property (nonatomic, retain) UIView*   parentView;

+ (InGameBrowserNative *)sharedInGameBrowserNative;

-(void)openCustomLink:(NSString*)customLinkString withPostString:(NSString*)postString;

@end