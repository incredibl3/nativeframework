#include "InGameBrowserNative.h"

@implementation WebViewPlugin

InGameBrowserNative* s_inGameBrowser = nil;

// The plugin must call super dealloc.
- (void) dealloc {
    self.appDelegate = nil;
    
    [super dealloc];
}

// The plugin must call super init.
- (id) init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.appDelegate = nil;

	if (s_inGameBrowser != nil)
	{
		// Alert("InGameBrowser::Init will be ignored. Library is already initialized");	
	}    
   
	s_inGameBrowser = [InGameBrowserNative sharedInGameBrowserNative];
	TeaLeafViewController* controller = (TeaLeafViewController*)[[[UIApplication sharedApplication] keyWindow] rootViewController];
	s_inGameBrowser.parentView = controller.view;

    return self;
}

- (void) openURL: (NSDictionary *)jsonObject {
	NSLOG(@"{webview} openURL: %@", jsonObject[@"url"]);
	NSString* url = [self ResolveUrl:jsonObject[@"url"]];
	NSString* ori = jsonObject[@"orientation"];
	if(!ori) {
		NSLog(@"Tiendv - no orientation is given!!!");
		ori = @"landscape";
	}
	[s_inGameBrowser openGame:url orientation:ori];
}

- (NSString*) ResolveUrl: (NSString*)url {
	NSString* baseUrl = @"http://128.199.109.41:3100/apps/";
	baseUrl = [[baseUrl stringByAppendingString:url] stringByAppendingString:@"/"];
	return baseUrl;
}

@end