#import "InGameBrowserNative.h"
#import "SynthesizeSingletonIGB.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#include <cassert>
#include <atomic>

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation InGameBrowserNative
{
	NSURLConnection*  directLinkConnection;
	NSMutableData*    directLinkData;
	std::atomic<bool> insideDirectLinkQuery;
	void (*_facebookShareLinkCallback)(const char*);
}

SYNTHESIZE_SINGLETON_FOR_CLASS(InGameBrowserNative)
// @synthesize isInIGBrowser = _isInIGBrowser;

- (id) init {
	self = [super init];

	if (self) {
        CGSize screenSize = [UIScreen mainScreen].bounds.size;

        _screenWidth    = screenSize.width;
        _screenHeight   = screenSize.height;

        _reloadButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh
                                                                      target:self
                                                                      action:@selector(reload)];

		UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];

        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            [activityView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
        }

        [activityView startAnimating];
        _loadingButton = [[UIBarButtonItem alloc] initWithCustomView:activityView];
        [activityView release];
        
        _forwardButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay
                                                                       target:self
                                                                       action:@selector(goForward)];
        _forwardButton.enabled = NO;
        
        _backButton = [self backButton];//[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRewind
        //												  target:self
        //												  action:@selector(goBack)];
        [_backButton retain];
        _backButton.enabled = NO;
        
        _flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
        _fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:NULL];
	}

	return self;
}

- (void) openGame:(NSString*)customLinkString orientation:(NSString*)orientation {
	_IGBView = [[UIView alloc] init];
    _IGBWebView = [[UIWebView alloc] init];
    _navBar = [[UINavigationBar alloc] init];
    _navBarBack = [[UIBarButtonItem alloc] initWithTitle:@"Tiendv" style:UIBarButtonItemStyleDone target:self action:@selector(quitIGB)];

    [_IGBView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1]];
    [_IGBView setOpaque:YES];    
	
	_IGBView.layer.zPosition = 100;

	NSURL *url = [NSURL URLWithString:customLinkString];
	NSLog(@"url = %@", url);	
	NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];

    [_IGBWebView setBackgroundColor:[UIColor clearColor]];
    [_IGBWebView setOpaque:NO];
    [_IGBWebView loadRequest:requestObj];
    
	[_IGBWebView setDelegate:self];	

    for (id subview in _IGBWebView.subviews)
        if ([[subview class] isSubclassOfClass:[UIScrollView class]])
            [((UIScrollView*)subview) setBounces:NO];
    
    [_IGBView addSubview:_IGBWebView];

    UINavigationItem* item = [[UINavigationItem alloc] initWithTitle:@""];
    item.leftBarButtonItem = _navBarBack;
    [_navBar pushNavigationItem:item animated:NO];
    [item release];
    [_navBarBack release];
    [_IGBView addSubview:_navBar];
    
    _toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, _screenWidth - 44, _screenHeight, 44)];
    [_toolbar setFrame:CGRectMake(0, _screenWidth - 44, _screenHeight, 44)];

	_forwardButton.enabled = NO;
    _backButton.enabled = NO;
	
	// Flexible space	
    _fixedSpace.width = 40.0f;
	
	// Assign buttons to toolbar
	_toolbar.items = [NSArray arrayWithObjects:_backButton, _fixedSpace,_forwardButton, _flexibleSpace, _reloadButton, nil];
    
    // [_IGBView addSubview:_toolbar];
    [_parentView addSubview:_IGBView];
        
    [self setOrientation: orientation];	
}

- (void)setOrientation:(NSString*) orientation
{
//    if(!_isInIGBrowser)
//        return;

    // UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    
    if([orientation isEqualToString:@"landscape"] == 1)    // 1
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [_IGBWebView setFrame:CGRectMake(0, 32, _screenHeight, _screenWidth -32 -44)];
            [_navBar setFrame:CGRectMake(0, 0, _screenHeight, 32)];
            [_toolbar setFrame:CGRectMake(0, _screenWidth - 44, _screenHeight, 44)];
        }
        else {
            [_IGBWebView setFrame:CGRectMake(0, 44, _screenHeight, _screenWidth - 44 - 44)];
            [_navBar setFrame:CGRectMake(0, 0, _screenHeight, 44)];
            [_toolbar setFrame:CGRectMake(0, _screenWidth - 44, _screenHeight, 44)];
        }

        NSLog(@"Tiendv: UIInterfaceOrientat`ionLandscapeLeft");
        // if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            _IGBView.transform = CGAffineTransformMakeRotation(M_PI/2);
        }
        
        // if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            [_IGBView setCenter:CGPointMake((_screenWidth )/2, _screenHeight/2)];
        }
        // else
        // {
        //     [_IGBView setCenter:CGPointMake(_screenHeight/2, (_screenWidth )/2)];
        // }

        [_IGBView setBounds:CGRectMake(0, 0, _screenHeight, _screenWidth)];

    }
    else {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            [_IGBWebView setFrame:CGRectMake(0, 32, _screenWidth, _screenHeight -32 -44)];
            [_navBar setFrame:CGRectMake(0, 0, _screenWidth, 32)];
            [_toolbar setFrame:CGRectMake(0, _screenHeight - 44, _screenWidth, 44)];
        }
        else {
            [_IGBWebView setFrame:CGRectMake(0, 44, _screenWidth, _screenHeight -32 -44)];
            [_navBar setFrame:CGRectMake(0, 0, _screenWidth, 32)];
            [_toolbar setFrame:CGRectMake(0, _screenHeight - 44, _screenWidth, 44)];
        }

        NSLog(@"Tiendv: UIInterfaceOrientationPortrait");
        if(!SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
        {
            _IGBView.transform = CGAffineTransformMakeRotation(0);
        }
        [_IGBView setCenter:CGPointMake(_screenWidth/2, _screenHeight/2)];
        [_IGBView setBounds:CGRectMake(0, 0, _screenWidth, _screenHeight)];
    }
}

- (CGContextRef)createContext
{
    // create the bitmap context
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(nil,27,27,8,0,
                                                 colorSpace,kCGImageAlphaPremultipliedLast);
    CFRelease(colorSpace);
    return context;
}


- (CGImageRef)createBackArrowImageRef
{
    CGContextRef context = [self createContext];
    
    // set the fill color
    CGColorRef fillColor = [[UIColor blackColor] CGColor];
    CGContextSetFillColor(context, CGColorGetComponents(fillColor));
    
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, 8.0f, 12.0f);
    CGContextAddLineToPoint(context, 24.0f, 3.0f);
    CGContextAddLineToPoint(context, 24.0f, 21.0f);
    CGContextClosePath(context);
    CGContextFillPath(context);
    
    // convert the context into a CGImageRef
    CGImageRef image = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    
    return image;
}


- (UIBarButtonItem *)backButton
{
    CGImageRef theCGImage = [self createBackArrowImageRef];
    UIImage *backImage = [[UIImage alloc] initWithCGImage:theCGImage];
    CGImageRelease(theCGImage);
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:backImage
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(goBack)];
    
    [backImage release], backImage = nil;
    
    return [backButton autorelease];
}

- (void)quitIGB
{
    NSLog(@"Tiendv quitIGB");
    if (insideDirectLinkQuery == YES)
    {
        [directLinkConnection cancel];
        [directLinkData release];
        directLinkData = nil;
        [directLinkConnection release];
        directLinkConnection = nil;
    }
    
    [_IGBWebView stopLoading];
    [_IGBWebView setDelegate:nil];
    [_IGBWebView removeFromSuperview];
    [_IGBWebView release];
    _IGBWebView = nil;
    [_navBar removeFromSuperview];
    [_navBar release];
    [_toolbar removeFromSuperview];
    [_toolbar release];
    
    
    [_IGBView removeFromSuperview];
    [_IGBView release];
    _IGBView = nil;
}

@end