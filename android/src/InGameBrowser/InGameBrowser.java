package com.tealeaf.plugin.plugins;

import java.io.*;
import java.util.*;

import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;

import org.json.*;

import android.app.*;
import android.content.*;
import android.content.pm.*;
import android.graphics.*;
import android.graphics.*;
import android.net.*;
import android.net.http.SslError;
import android.os.*;
import android.util.*;
import android.view.*;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.inputmethod.*;
import android.view.View.OnTouchListener;
import android.webkit.*;
import android.widget.*;
import android.text.TextUtils;

import com.tealeaf.plugin.plugins.WebViewPlugin;
import com.example.studio.tabbed.R;

public class InGameBrowser extends Activity {
	private WebView mWebView = null;
	private RelativeLayout mView = null;
	public final static String HOST = "http://128.199.109.41:3100/apps/";
	private int SCR_W = 0;
	private int SCR_H = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_in_game_browser);
		
		Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
		SCR_W = display.getWidth();
		SCR_H = display.getHeight();

		String orientation = getIntent().getStringExtra(WebViewPlugin.GAME_ORIENTATION);
		if (orientation.equals("portrait")) {
			setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);	
		} else {
			setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);	
		}
		
		RelativeLayout mWebLayout = ((RelativeLayout)findViewById(R.id.ingamebrowser_webview));
		mWebView = new WebView(this);
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setLoadWithOverviewMode(true);
		mWebView.getSettings().setUseWideViewPort(false);
		mWebView.getSettings().setBuiltInZoomControls(false);
		mWebView.getSettings().setSupportZoom(true);
		mWebView.getSettings().setDomStorageEnabled(true);
		mWebView.setWebViewClient(new IGBWebViewClient());
		mWebView.setWebChromeClient(new WebChromeClient());

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			mWebView.setWebContentsDebuggingEnabled(true);
		}		

		mWebLayout.addView(mWebView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);		

		String url = getIntent().getStringExtra(WebViewPlugin.OPEN_URL);
		url = ResolveUrl(url);

		android.util.Log.d("Tiendv", "Get Url: " + url);

		mWebView.loadUrl(url);
	}

	private String ResolveUrl(String url) {
		return HOST + url + "/";
	}

	class IGBWebViewClient extends WebViewClient {
		@Override
		public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
			handler.proceed();
		}
		
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			// DBG("INGAMEBROWSER", "======= onPageStarted =======\n" + url);
			// showProgressLoading();
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			// DBG("INGAMEBROWSER", "======= onPageFinished ========\n" + url);
			// hideProgress();
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return true;
		}

		@Override
		public void onReceivedError(final WebView view, int errorCode, final String description, String failingUrl) {
			
		}
	}

}