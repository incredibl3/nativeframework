package com.tealeaf.plugin.plugins;
import com.tealeaf.TeaLeaf;
import com.tealeaf.logger;
import com.tealeaf.plugin.IPlugin;
import com.tealeaf.EventQueue;
import com.tealeaf.event.*;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;
import android.content.pm.PackageManager;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import android.widget.FrameLayout;
import android.view.Gravity;
import android.net.Uri;

public class WebViewPlugin implements IPlugin {
	public class WebViewPluginEvent extends com.tealeaf.event.Event {
		public static final int BANNER_CLOSED = 0;
		public static final int INTERSTITIAL_CLOSED = 1;
		public static final int INTERSTITIAL_NOTREADY = 2;
		int eventID;

		public WebViewPluginEvent(int code) {
			super("webviewplugin");
			this.eventID = code;
		}
	}
	private String TAG = "{webview}";
	public final static String OPEN_URL = "com.incredibl3.WebViewPlugin.OPEN_URL";
	public final static String GAME_ORIENTATION = "com.incredibl3.WebViewPlugin.GAME_ORIENTATION";

	private Activity mActivity;

	public void WebViewPlugin() {}

	public void onCreateApplication(Context applicationContext) {

	}


	public void onCreate(Activity activity, Bundle savedInstanceState) {
		mActivity = activity;
	}

	public void onResume() {}

	public void onStart() {}

	public void onPause() {}

	public void onStop() {}

	public void onDestroy() {

	}

	public void onNewIntent(Intent intent) {}

	public void setInstallReferrer(String referrer) {}

	public void onActivityResult(Integer request, Integer result, Intent data) {}

	public boolean consumeOnBackPressed() {
		return true;
	}

	public void onBackPressed() {}

	public void openURL(String jsonData) {
		JSONObject jObject = null;
		try {
			jObject = new JSONObject(jsonData);
		} catch (Exception ex) {
			logger.log(TAG, ex.toString());
		}
		String url = getJsonString(jObject, "url");
		String orientation = getJsonString(jObject, "orientation");
		android.util.Log.d("Tiendv", "urL: " + url + "| orientation: " + orientation);

		if (orientation.equals("")) {
			android.util.Log.d("Tiendv", "No orientation is given!!");
			orientation = "landscape";
		}

		Intent intent = new Intent(mActivity, InGameBrowser.class);
		intent.putExtra(OPEN_URL, url);
		intent.putExtra(GAME_ORIENTATION, orientation);
		mActivity.startActivity(intent);
	}

	private static String getJsonString(JSONObject jObject, String key) {
		if (jObject == null) {
			return "";
		}

		String res = "";
		try {
			res = jObject.getString(key);
		} catch (Exception ex) {}
		return res;
	}	
}
