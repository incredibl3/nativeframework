var webview = Class(function () {
	
	this.openURL = function (opts) {
        NATIVE.plugins.sendEvent("WebViewPlugin", "openURL", JSON.stringify(opts));
    };
});

exports = new webview();